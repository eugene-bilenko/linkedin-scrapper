from django import forms
from django.utils.text import slugify

from linkedin.models import Profile, Certification


class ProfileForm(forms.ModelForm):
    page_url = forms.URLField(required=True)
    full_name = forms.CharField(max_length=255)
    summary = forms.CharField(required=False)
    company_name = forms.CharField(max_length=255, required=False)
    job_title = forms.CharField(max_length=255, required=False)
    location = forms.CharField(max_length=255, required=False)
    connections_count = forms.CharField(max_length=255, required=False)

    class Meta:
        model = Profile
        fields = ('summary', 'page_url', 'full_name', 'company_name', 'job_title', 'location', 'connections_count',)


class CertificationForm(forms.ModelForm):
    accomplishment_title = forms.CharField(max_length=255, required=True)
    certification_authority = forms.CharField(max_length=255, required=False)
    date = forms.CharField(max_length=255, required=False)
    slug = forms.CharField(max_length=255, required=False)

    def __init__(self, data=None, *args, **kwargs):
        data['slug'] = slugify(data['accomplishment_title'])
        super().__init__(data, *args, **kwargs)


    def clean(self):
        data = {}

        for key, value in self.cleaned_data.items():
            data[key] = value.strip()

        return data

    class Meta:
        model = Certification
        fields = ('slug','accomplishment_title', 'certification_authority', 'date',)
