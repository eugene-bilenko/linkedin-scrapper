import json

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from linkedin.forms import ProfileForm, CertificationForm
from linkedin.models import Profile


def check(cleaned_data):
    data = {}
    has_keyword = False

    for key, value in cleaned_data.items():
        if isinstance(value, list):
            for d in value:
                if check(d):
                    has_keyword = True
        else:
            data[key] = value.strip()
            if 'workday' in value.lower():
                has_keyword = True

    return has_keyword


@csrf_exempt
def accept_profile(request):
    data = json.loads(request.body.decode('utf-8'))

    profile = Profile.objects.filter(page_url=data.get('page_url')).first()

    print("Profile: ", data.get('full_name'))

    if not check(data):
        return JsonResponse({'error': 'No Workday keyword.'})

    if not profile:
        profile_form = ProfileForm(data)
    else:
        profile_form = ProfileForm(data, instance=profile)

    try:
        profile = profile_form.save()
    except Exception as e:
        print(str(e))

    for cert in data.get('certifications', []):
        try:
            certification_form = CertificationForm(cert)
            if certification_form.is_valid():
                certification = certification_form.save(commit=False)
                certification.profile = profile
                certification.save()
            else:
                print(certification_form.errors)
        except Exception as e:
            print(str(e))

    return JsonResponse({})
