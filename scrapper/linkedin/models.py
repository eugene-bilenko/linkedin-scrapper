from django.db import models


class Profile(models.Model):
    page_url = models.URLField(unique=True)
    full_name = models.CharField(max_length=255)
    summary = models.TextField(null=True, blank=True)
    company_name = models.CharField(max_length=255, null=True, blank=True)
    job_title = models.CharField(max_length=255, null=True, blank=True)
    location = models.CharField(max_length=255, null=True, blank=True)
    connections_count = models.CharField(max_length=255, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.full_name


class Certification(models.Model):
    profile = models.ForeignKey(Profile, related_name='certifications')
    slug = models.CharField(max_length=255)
    accomplishment_title = models.CharField(max_length=255, null=True, blank=True)
    certification_authority = models.CharField(max_length=255, null=True, blank=True)
    date = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.certification_authority

    class Meta:
        unique_together = ('profile', 'slug',)
