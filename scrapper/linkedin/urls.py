from django.conf.urls import url

import linkedin.views as views

urlpatterns = [
    url(r'^save-profile/$', views.accept_profile, name='save-profile'),
]
