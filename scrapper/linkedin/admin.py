from django.contrib import admin
from django.contrib.admin import TabularInline
from import_export.admin import ExportMixin

from linkedin.models import Profile, Certification
from linkedin.resources import ProfileResource


class CertificationInlineAdmin(TabularInline):
    model = Certification
    extra = 0


@admin.register(Profile)
class ProfileAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = ProfileResource
    list_display = ('pk', 'full_name', 'page_url', 'connections_count', 'company_name', 'job_title', 'location', 'created_at',)
    list_display_links = ('pk', 'full_name',)
    search_fields = ('full_name',)
    list_filter = ('created_at',)
    inlines = (CertificationInlineAdmin,)


@admin.register(Certification)
class CertificationAdmin(admin.ModelAdmin):
    list_display = ('pk', 'profile', 'accomplishment_title', 'certification_authority', 'date',)
    list_display_links = ('pk',)
    search_fields = ('accomplishment_title', 'certification_authority',)
