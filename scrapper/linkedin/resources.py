from import_export import fields
from import_export.resources import ModelResource

from linkedin.models import Profile


class ProfileResource(ModelResource):
    certifications = fields.Field()

    def dehydrate_certifications(self, inst: Profile):
        rows = [
            '{%s}' % '|'.join((cert.accomplishment_title, cert.certification_authority, cert.date,))
            for cert in inst.certifications.all()
        ]
        return ';'.join(rows)

    class Meta:
        model = Profile
        fields = [
            'id', 'page_url', 'full_name', 'company_name', 'job_title', 'location', 'connections_count', 'created_at',
            'certifications',
        ]
        export_order = [
            'id', 'page_url', 'full_name', 'company_name', 'job_title', 'location', 'connections_count', 'created_at',
            'certifications',
        ]
