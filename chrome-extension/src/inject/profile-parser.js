(function () {
    "use strict";

    let pageData = null;
    window.last_url = '';

    // window.addEventListener('scroll', function() {
    //     document.querySelectorAll('.search-result__occluded-item .actor-name.name').forEach(function(node) {
    //         if(!node.marked) {
    //             node.marked = true;
    //             node.innerHTML = `<span style="color: cadetblue">(public)</span> ${node.textContent}`;
    //         }
    //     });
    // });

    function query(selector) {
        return document.querySelector(selector);
    }

    function getContent(el) {
        if(el) return el.textContent;
        return '';
    }

    function parseCertifications() {
        let $entities = document.querySelectorAll('.certifications .pv-accomplishment-entity');
        let certifications = [];
        $entities.forEach(function (node) {
            let $title = node.querySelector('.pv-accomplishment-entity__title');
            let $authority = node.querySelector('[data-control-name="certification_detail_company"] p');
            let $date = node.querySelector('.pv-accomplishment-entity__date');

            if($title) $title.children.item(0).remove();
            if($authority) $authority.children.item(0).remove();
            if($date) $date.children.item(0).remove();

            let accomplishment_title = $title ? $title.textContent : '';
            let certification_authority = $authority ? $authority.textContent : '';
            let date = $date ? $date.textContent : '';

            certifications.push({
                'accomplishment_title': accomplishment_title,
                'certification_authority': certification_authority,
                'date': date,
            });
        });
        return certifications;
    }

    function parsePage(certifications) {
        console.log('Page loaded. Start parsing...');
        pageData = {
            'full_name': getContent(query('.pv-top-card-section__name')),
            'page_url': `${document.location.origin}${document.location.pathname}`,
            'company_name': getContent(query('.pv-top-card-section__company')),
            'job_title': getContent(query('.pv-top-card-section__headline')),
            'location': getContent(query('.pv-top-card-section__location')),
            'connections_count': getContent(query('.pv-top-card-section__connections span[aria-hidden="true"]')),
            'summary': getContent(query('.pv-top-card-section__summary .truncate-multiline--last-line')),
            'certifications': certifications
        };

        return pageData;
    }

    function sendData(pageData) {
        return fetch("https://parse.devhost1.com/api/save-profile/", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            //make sure to serialize your JSON body
            body: JSON.stringify(pageData)
        });
    }

    chrome.runtime.onMessage.addListener(function (event, extension, response) {
        if(event.type === 'page.getData') {
            response(pageData);
        }
    });

    let timeout = 15;
    let time = 0;
    let inFocus = false;

    window.onfocus = function () {
        inFocus = true;
    };

    setInterval(function () {
        if(window.last_url !== document.location.pathname && document.location.pathname.startsWith('/in/')) {
            pageData = null;
            let $footer = query('.nav-footer');
            let $certs_section_btn = query('[data-control-name="accomplishments_expand_certifications"]');
            // If page loaded - scroll to bottom
            if($footer !== null)
                $footer.scrollIntoViewIfNeeded();

            // If expand certifications button exists - click it
            if($certs_section_btn !== null)
                $certs_section_btn.click();

            // When all sections expanded - parse and save document
            if (query('.pv-accomplishments-block--expanded') !== null || time >= timeout) {
                window.last_url = document.location.pathname;
                let certs = parseCertifications();
                let data = parsePage(certs);
                sendData(data).then(function () {
                    window.close();
                });
                time = 0;
            }
            if(inFocus)
                time += 1;
        }
    }, 1000);

    setInterval(function () {
        document.querySelectorAll('.search-result__occluded-item').forEach(function(node) {
            let actor = node.querySelector('.actor-name.name');
            let link = node.querySelector('.search-result__result-link');
            if(!node.marked && link && actor) {
                node.marked = true;
                actor.innerHTML = `<span style="color: cadetblue">(public)</span> ${actor.textContent}`;
                window.open(link.href);
            }
        });
    }, 1000);

})();