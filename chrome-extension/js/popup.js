(function () {
    "use strict";

    function query(selector) {
        return document.getElementById(selector);
    }

    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(tabs) {

        chrome.tabs.sendMessage(tabs[0].id, {type: 'page.getData'}, function (pageData) {
            if(!pageData) {
                document.getElementById('no-info').classList.remove('hidden');
                document.getElementById('profile-container').classList.add('hidden');
                return;
            }
            document.getElementById('no-info').classList.add('hidden');
            document.getElementById('profile-container').classList.remove('hidden');
            query('page-url').textContent = pageData.page_url;
            query('page-url').href = pageData.page_url;
            query('full-name').textContent = pageData.full_name;
            query('company-name').textContent = pageData.company_name;
            query('job-title').textContent = pageData.job_title;
            query('location').textContent = pageData.location;
            query('connections-count').textContent = pageData.connections_count;
            query('summary').textContent = pageData.summary;
        });
    });

})();